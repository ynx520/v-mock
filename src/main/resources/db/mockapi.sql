/*
 Navicat Premium Data Transfer

 Source Server         : rm-bp1jtgrd4ssy9g3tzgo.mysql.rds.aliyuncs.com
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : rm-bp1jtgrd4ssy9g3tzgo.mysql.rds.aliyuncs.com:3306
 Source Schema         : mockapi

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 14/11/2022 17:39:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mock_log
-- ----------------------------
DROP TABLE IF EXISTS `mock_log`;
CREATE TABLE `mock_log`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_method` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `request_ip` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `hit_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `request_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `response_detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `request_detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mock_log
-- ----------------------------


-- ----------------------------
-- Table structure for mock_response
-- ----------------------------
DROP TABLE IF EXISTS `mock_response`;
CREATE TABLE `mock_response`  (
  `response_id` int(11) NOT NULL AUTO_INCREMENT,
  `url_id` int(11) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status_code` int(11) NULL DEFAULT NULL,
  `main` int(11) NULL DEFAULT 0,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `custom_header` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  `create_by` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `method` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`response_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mock_response
-- ----------------------------
INSERT INTO `mock_response` VALUES (1, 1, '<h1>示例接口请求成功~23333</h1>', 200, 1, '示例接口默认返回体', '[{\"key\":\"content-type\",\"val\":\"text/html;charset=utf-8\"}]', 0, '1', '2022-11-14 17:19:26', 1, NULL, NULL, 'GET');

-- ----------------------------
-- Table structure for mock_response_restful
-- ----------------------------
DROP TABLE IF EXISTS `mock_response_restful`;
CREATE TABLE `mock_response_restful`  (
  `response_id` int(11) NOT NULL AUTO_INCREMENT,
  `url_id` int(11) NOT NULL,
  `http_method` int(11) NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status_code` int(11) NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `custom_header` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`response_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mock_response_restful
-- ----------------------------
INSERT INTO `mock_response_restful` VALUES (3, 1, 1, '{\"msg\" : \"success\", \"code\": 666}', 200, 'HEAD 方法的响应不会包含响应正文. 所以返回体可能被调用方忽略。', '', 0, 1, NULL, 1, 2147483647, NULL);
INSERT INTO `mock_response_restful` VALUES (4, 1, 2, '{\"text\": \"创建成功\", \r\n\"code\" : 10001\r\n}', 200, '创建资源', '', 0, 1, NULL, 1, 2147483647, NULL);
INSERT INTO `mock_response_restful` VALUES (5, 1, 0, '<h1>示例Restful接口请求成功(GET)！！</h1>', 200, '获取资源', '[{\"key\":\"content-type\",\"val\":\"text/html;charset=utf-8\"},{\"key\":\"cookies\",\"val\":\"vmock=test\"}]', 0, 1, 2147483647, 1, 2147483647, NULL);

-- ----------------------------
-- Table structure for mock_url
-- ----------------------------
DROP TABLE IF EXISTS `mock_url`;
CREATE TABLE `mock_url`  (
  `url_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `logic` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `response_type` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`url_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mock_url
-- ----------------------------
INSERT INTO `mock_url` VALUES (1, '/example', '示例接口', '示例请求接口, 已启用接收GET的响应体\r\n\r\n访问ip:port/vmock/example即可访问到该接口', '1', 0, 1, NULL, 1, NULL, NULL, 1);

-- ----------------------------
-- Table structure for mock_url_logic
-- ----------------------------
DROP TABLE IF EXISTS `mock_url_logic`;
CREATE TABLE `mock_url_logic`  (
  `logic_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`logic_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mock_url_logic
-- ----------------------------
INSERT INTO `mock_url_logic` VALUES (1, 'example', 0, 1, 2147483647, 1, 2147483647, NULL);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `config_key` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `config_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '邀请码', 'system.invitation.code', 'vmock', 1, 2147483647, 1, 2147483647, '用户由前台自己注册系统时，需要输入该邀请码验证身份，才能注册成功。', 0);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` int(11) NOT NULL AUTO_INCREMENT,
  `dict_sort` int(11) NULL DEFAULT NULL,
  `dict_label` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `dict_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `dict_type` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `css_class` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `list_class` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `is_default` int(11) NULL DEFAULT NULL,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 0, 1, 2018, 1, 2018, '性别男', 0);
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 0, 1, 2018, 1, 2018, '性别女', 0);
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 0, 1, 2018, 1, 2018, '性别未知', 0);
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 0, 1, 2018, 1, 2018, '显示菜单', 0);
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 0, 1, 2018, 1, 2018, '隐藏菜单', 0);
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 0, 1, 2018, 1, 2018, '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 0, 1, 2018, 1, 2018, '停用状态', 0);
INSERT INTO `sys_dict_data` VALUES (8, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 0, 1, 2018, 1, 2018, '系统默认是', 0);
INSERT INTO `sys_dict_data` VALUES (9, 2, '否', 'N', 'sys_yes_no', '', 'danger', 0, 1, 2018, 1, 2018, '系统默认否', 0);
INSERT INTO `sys_dict_data` VALUES (10, 1, '成功', '0', 'sys_common_status', '', 'primary', 0, 1, 2018, 1, 2018, '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (11, 2, '失败', '1', 'sys_common_status', '', 'danger', 0, 1, 2018, 1, 2018, '停用状态', 0);
INSERT INTO `sys_dict_data` VALUES (12, 0, 'GET', 'GET', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);
INSERT INTO `sys_dict_data` VALUES (13, 1, 'HEAD', 'HEAD', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);
INSERT INTO `sys_dict_data` VALUES (14, 2, 'POST', 'POST', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);
INSERT INTO `sys_dict_data` VALUES (15, 3, 'PUT', 'PUT', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);
INSERT INTO `sys_dict_data` VALUES (16, 4, 'PATCH', 'PATCH', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);
INSERT INTO `sys_dict_data` VALUES (17, 5, 'DELETE', 'DELETE', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);
INSERT INTO `sys_dict_data` VALUES (18, 6, 'OPTIONS', 'OPTIONS', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);
INSERT INTO `sys_dict_data` VALUES (19, 7, 'TRACE', 'TRACE', 'http_method', '', '', 0, 1, 2147483647, 1, 2147483647, 'http请求方法', 0);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `dict_type` int(11) NULL DEFAULT NULL,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` int(11) NULL DEFAULT NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 0, 1, 2018, 1, 2018, 0, NULL);
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 0, 1, 2018, 1, 2018, 0, NULL);
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 0, 1, 2018, 1, 2018, 0, NULL);
INSERT INTO `sys_dict_type` VALUES (4, '系统是否', 0, 1, 2018, 1, 2018, 0, NULL);
INSERT INTO `sys_dict_type` VALUES (5, '系统状态', 0, 1, 2018, 1, 2018, 0, NULL);
INSERT INTO `sys_dict_type` VALUES (6, '请求方式', 0, 1, 2147483647, 1, 2147483647, 0, 0);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `target` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `menu_type` int(11) NULL DEFAULT NULL,
  `visible` int(11) NULL DEFAULT NULL,
  `icon` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_by` varchar(64) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `user_display` int(11) NULL DEFAULT 1,
  `del_flag` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '用户管理', 0, 1, '/system/user', 'menuItem', 0, 0, 'fa fa-user', 1, 2147483647, 1, 2147483647, '用户管理菜单', 0, 0);
INSERT INTO `sys_menu` VALUES (2, '系统参数', 0, 7, '/system/config', 'menuItem', 0, 0, 'fa fa-cogs', 1, 2147483647, 1, 2147483647, '参数设置菜单', 0, 0);
INSERT INTO `sys_menu` VALUES (3, '接口日志', 0, 2, '/system/log', 'menuItem', 0, 0, 'fa fa-book', 1, 2147483647, 1, 2147483647, '操作日志菜单', 1, 0);
INSERT INTO `sys_menu` VALUES (4, '接口一览', 0, 1, '/system/url', 'menuItem', 0, 0, 'fa fa-cloud', 1, 2147483647, 1, 2147483647, 'url路径菜单', 1, 0);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `user_type` int(11) NULL DEFAULT 0,
  `email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `phonenumber` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `sex` int(11) NULL DEFAULT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `salt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` int(11) NULL DEFAULT 0,
  `del_flag` int(11) NULL DEFAULT 0,
  `login_ip` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `login_date` datetime(0) NULL DEFAULT NULL,
  `create_by` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'vmock', '超级管理员', 1, '', '13999999999', 1, '', '568611dc5efd140df77f58d5ae7c84f6', 'fc4a41', 0, 0, '0:0:0:0:0:0:0:1', '2022-11-14 17:38:55', '0', '2022-11-14 17:25:29', '1', '2022-11-14 17:38:55', '管理员，能多看见【用户管理】和【系统参数】');

SET FOREIGN_KEY_CHECKS = 1;
