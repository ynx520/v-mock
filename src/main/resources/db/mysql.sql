--
-- 由SQLiteStudio v3.3.3 产生的文件 周一 11月 14 15:46:59 2022
--
-- 文本编码：UTF-8
--

-- 表：mock_log
DROP TABLE IF EXISTS mock_log;

CREATE TABLE mock_log (
    log_id          INTEGER NOT NULL
                            PRIMARY KEY AUTO_INCREMENT,
    request_method  TEXT,
    request_ip      TEXT,
    hit_url         TEXT,
    request_url     TEXT,
    response_detail TEXT,
    request_detail  TEXT,
    del_flag        INTEGER DEFAULT 0,
    create_by       INTEGER,
    create_time     INTEGER,
    update_by       INTEGER,
    update_time     INTEGER,
    remark          TEXT
);

INSERT INTO mock_log (
                         log_id,
                         request_method,
                         request_ip,
                         hit_url,
                         request_url,
                         response_detail,
                         request_detail,
                         del_flag,
                         create_by,
                         create_time,
                         update_by,
                         update_time,
                         remark
                     )
                     VALUES (
                         1,
                         'GET',
                         '0:0:0:0:0:0:0:1',
                         '/example',
                         '/example',
                         '{"respHeader":[{"val":"text/html;charset=utf-8","key":"content-type"}],"respStatus":200,"respContent":"<h1>示例接口请求成功~<\/h1>"}',
                         '{"headers":{"sec-fetch-mode":"navigate","sec-fetch-site":"none","accept-language":"zh-CN,zh;q=0.9","cookie":"liveagent_oref=; liveagent_vc=2; liveagent_ptid=087fe696-cea7-47f2-948f-6038217d3ae6; Hm_lvt_8875c662941dbf07e39c556c8d97615f=1574836829; rememberMe=hj7o65BICkGO3PQixR+8Jd0eZLHuSBn4LY/dUCP2zEuuXRFMuIRz5PfPnZUrwbaCkYIMzJkEpmhRAJDAImsDajk3cAcNUM/oJW29UQFD3h3ZEC0MOnrD8Up/gprGT1UvwDO8h57jCA2Muv+Kxr4UZY3HDt5Ajb+Tp56k8+Y6u7T9JbBRNR6MZdPQVOFpkKzY3ONfLB1oKFCkoOVv6henkoM+cjz3zJN+wkdorQ2UXc2xGzWj17IiAId3H6MtArgUrYKdU1SowZdnjBtmgLF5aUFfd8rYIROhR1oUo0lvzmBFeozUfFGEYcEk4AadC0AaEI0x+z34fPf1dS82PdWPbiamG4m722nO7H9ZEpz6MN+A9bSkc3kWAs0ZxxBoWWht1h6jfKeA4BErOsA08EUsbsmMpBusYbymKn3qyVKSQOGBeacPEPGGS5t7UnU0MRPtGlkuJJn4zANzfML8FV2mzY4xcMFainkLHARveLWDwDZBOPv1MNWCwVHk0O2zd+Bljt8FWLY4kWaGi/h9G+ljga9QmgV2UQBojycUpofM+dsZ3g+T82n2AlFyjS0ZA4XG3Ie88fpDfwlXxDn89vQ/Fxixly0lgzQZWU64C4Ft/LlU0ZevIH1E3emhUkhHEXclqIamEji1ATGHc8J/5HzU339+E7x5RnlCb4c0taX0tTsObJly3T2x3Lw1lliX56RzZLyDI7IohAjTeQO5ls7GUjuwVObzkISd76Lpd2UsFqhhOFGc4vI7o+ggNk53vPYswM/7YkU7mIQM/b8N+tR9OyAPLRV9aIqu/wKjzmr11u81BY5+52nm+1qf26EayYAwfA6f9/riaIw5ncycxpW+084n8TIhzYMSw8q/Zr8eDWc1v+PW2gM+E8tKkAgWj9BZbpAkrU0rTC+cajRvLOWo7dIWYCxrP3wblaOJpsuw8MnkTSpAvo0aWmxpsQcwmL1bBHWUuhC1kzub8nV1wdy77eJx2QSRxehy4Ijk9AlXMjUsGALs/75Vsh0y0hAI/pkq4diQw02Qke/NCeu+nm1qSzkUCX15nDhVAJLqntYJ1sMWR2hoqhyrQKZIGCeo5QY/XHNHTzx9jE582D4hnBaTzbKYQbUA8E/nfeNk8oK+dyzM19I27fJ+SN8ZxtiIuagvRcoMLI4XcEodj/rbGsg40fOjvhF4D5Qvrl+Jb+R3A9x+I7h2xWBQf9nFghNzBA/WcWwHui38ZKnxH6YMtmoKtZOpdHsMhW+dcYyS0pJ5x6EiLK6+ViKmaGCVjFYHXHIDW/8FqU7VIjUfipJPH12MVetVznolXMY1C2WIf9YKmXk6hUK8lBGzIdyaPI+PMyJd4MMUPzRoWUzcprDC6ccsz1auEWsaL2cInfZS6KbKCRNWFwVeRx1lxxZuaXUkQkLqc0o3E+AtlWzXd2VSWMxXo8oPDifhFrV/x6kSiGxSC5Cjglpfg6wmFUHMA3Ido8tfLzlgbS1d1Z67z9qps0tFuh+kYk05TFut71MWw58I571prmTD60rVbbwoJ9ILAKhul2etD1y5GCa7XzDpeJGsLjV7fqockMzY+XDS+2/BdPkGA2GLkBq8AmBVmLBvtkSoupDX5EfMPuiKTfV9V2rUfWJ2LMR21HIWWq+yB+9UD97cTULidnDQiiR1dzvr7cBAk4PdNH9slLr3LnZvvsxgOIOcVxreE7s2TT1GTiuxHVBlH0v3xfNHwkk++9lU94yFzfueUUo1awrAhToGZQgFcQ==; JSESSIONID=23C9BB99386A79B68218B472894CC993","host":"localhost","upgrade-insecure-requests":"1","connection":"keep-alive","accept-encoding":"gzip, deflate, br","user-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36","accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"},"params":{},"body":""}',
                         0,
                         NULL,
                         1577177166945,
                         NULL,
                         1577177166945,
                         NULL
                     );


-- 表：mock_response
DROP TABLE IF EXISTS mock_response;

CREATE TABLE mock_response (
    response_id   INTEGER NOT NULL
                          PRIMARY KEY AUTO_INCREMENT,
    url_id        INTEGER,
    content       TEXT,
    status_code   INTEGER,
    main          INTEGER DEFAULT 0,
    description   TEXT,
    custom_header TEXT,
    del_flag      INTEGER DEFAULT 0,
    create_by     INTEGER,
    create_time   INTEGER,
    update_by     INTEGER,
    update_time   INTEGER,
    remark        TEXT,
    method        TEXT
);

INSERT INTO mock_response (
                              response_id,
                              url_id,
                              content,
                              status_code,
                              main,
                              description,
                              custom_header,
                              del_flag,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              method
                          )
                          VALUES (
                              1,
                              1,
                              '<h1>示例接口请求成功~</h1>',
                              200,
                              1,
                              '示例接口默认返回体',
                              '[{"key":"content-type","val":"text/html;charset=utf-8"}]',
                              0,
                              1,
                              1577177140391,
                              1,
                              1577177410704,
                              NULL,
                              'GET'
                          );


-- 表：mock_response_restful
DROP TABLE IF EXISTS mock_response_restful;

CREATE TABLE mock_response_restful (
    response_id   INTEGER PRIMARY KEY AUTO_INCREMENT,
    url_id        INTEGER NOT NULL,
    http_method   INTEGER NOT NULL,
    content       TEXT,
    status_code   INTEGER,
    description   TEXT,
    custom_header TEXT,
    del_flag      INTEGER DEFAULT 0,
    create_by     INTEGER,
    create_time   INTEGER,
    update_by     INTEGER,
    update_time   INTEGER,
    remark        TEXT
);

INSERT INTO mock_response_restful (
                                      response_id,
                                      url_id,
                                      http_method,
                                      content,
                                      status_code,
                                      description,
                                      custom_header,
                                      del_flag,
                                      create_by,
                                      create_time,
                                      update_by,
                                      update_time,
                                      remark
                                  )
                                  VALUES (
                                      3,
                                      1,
                                      1,
                                      '{"msg" : "success", "code": 666}',
                                      200,
                                      'HEAD 方法的响应不会包含响应正文. 所以返回体可能被调用方忽略。',
                                      '',
                                      0,
                                      1,
                                      NULL,
                                      1,
                                      1589770482506,
                                      NULL
                                  );

INSERT INTO mock_response_restful (
                                      response_id,
                                      url_id,
                                      http_method,
                                      content,
                                      status_code,
                                      description,
                                      custom_header,
                                      del_flag,
                                      create_by,
                                      create_time,
                                      update_by,
                                      update_time,
                                      remark
                                  )
                                  VALUES (
                                      4,
                                      1,
                                      2,
                                      '{"text": "创建成功", 
"code" : 10001
}',
                                      200,
                                      '创建资源',
                                      '',
                                      0,
                                      1,
                                      NULL,
                                      1,
                                      1589770365834,
                                      NULL
                                  );

INSERT INTO mock_response_restful (
                                      response_id,
                                      url_id,
                                      http_method,
                                      content,
                                      status_code,
                                      description,
                                      custom_header,
                                      del_flag,
                                      create_by,
                                      create_time,
                                      update_by,
                                      update_time,
                                      remark
                                  )
                                  VALUES (
                                      5,
                                      1,
                                      0,
                                      '<h1>示例Restful接口请求成功(GET)！！</h1>',
                                      200,
                                      '获取资源',
                                      '[{"key":"content-type","val":"text/html;charset=utf-8"},{"key":"cookies","val":"vmock=test"}]',
                                      0,
                                      1,
                                      1589770119096,
                                      1,
                                      1589770380264,
                                      NULL
                                  );


-- 表：mock_url
DROP TABLE IF EXISTS mock_url;

CREATE TABLE mock_url (
    url_id        INTEGER    NOT NULL
                             PRIMARY KEY AUTO_INCREMENT,
    url           TEXT (255) NOT NULL,
    name          TEXT (150) NOT NULL,
    description   TEXT (255),
    logic         TEXT,
    del_flag      INTEGER    DEFAULT 0,
    create_by     INTEGER,
    create_time   INTEGER,
    update_by     INTEGER,
    update_time   INTEGER,
    remark        TEXT,
    response_type INTEGER    DEFAULT 1
);

INSERT INTO mock_url (
                         url_id,
                         url,
                         name,
                         description,
                         logic,
                         del_flag,
                         create_by,
                         create_time,
                         update_by,
                         update_time,
                         remark,
                         response_type
                     )
                     VALUES (
                         1,
                         '/example',
                         '示例接口',
                         '示例请求接口, 已启用接收GET的响应体
访问ip:port/vmock/example即可访问到该接口',
                         '1',
                         0,
                         1,
                         1577177041874,
                         1,
                         1577177041874,
                         NULL,
                         1
                     );


-- 表：mock_url_logic
DROP TABLE IF EXISTS mock_url_logic;

CREATE TABLE mock_url_logic (
    logic_id    INTEGER    PRIMARY KEY AUTO_INCREMENT
                           NOT NULL,
    sub_url     TEXT (200) NOT NULL,
    del_flag    INTEGER    DEFAULT 0,
    create_by   INTEGER,
    create_time INTEGER,
    update_by   INTEGER,
    update_time INTEGER,
    remark      TEXT,
    CONSTRAINT un_sub_url UNIQUE (
        sub_url COLLATE BINARY ASC
    )
    ON CONFLICT IGNORE
);

INSERT INTO mock_url_logic (
                               logic_id,
                               sub_url,
                               del_flag,
                               create_by,
                               create_time,
                               update_by,
                               update_time,
                               remark
                           )
                           VALUES (
                               1,
                               'example',
                               0,
                               1,
                               1577177041819,
                               1,
                               1577177041819,
                               NULL
                           );


-- 表：sys_config
DROP TABLE IF EXISTS sys_config;

CREATE TABLE sys_config (
    config_id    INTEGER NOT NULL
                         PRIMARY KEY AUTO_INCREMENT,
    config_name  TEXT,
    config_key   TEXT,
    config_value TEXT,
    create_by    INTEGER,
    create_time  INTEGER,
    update_by    INTEGER,
    update_time  INTEGER,
    remark       TEXT,
    del_flag     INTEGER DEFAULT 0
);

INSERT INTO sys_config (
                           config_id,
                           config_name,
                           config_key,
                           config_value,
                           create_by,
                           create_time,
                           update_by,
                           update_time,
                           remark,
                           del_flag
                       )
                       VALUES (
                           1,
                           '邀请码',
                           'system.invitation.code',
                           'vmock',
                           1,
                           1575425366734,
                           1,
                           1575958917712,
                           '用户由前台自己注册系统时，需要输入该邀请码验证身份，才能注册成功。',
                           0
                       );


-- 表：sys_dict_data
DROP TABLE IF EXISTS sys_dict_data;

CREATE TABLE sys_dict_data (
    dict_code   INTEGER PRIMARY KEY AUTO_INCREMENT,
    dict_sort   INTEGER,
    dict_label  TEXT,
    dict_value  TEXT,
    dict_type   TEXT,
    css_class   TEXT,
    list_class  TEXT,
    is_default  INTEGER,
    create_by   INTEGER,
    create_time INTEGER,
    update_by   INTEGER,
    update_time INTEGER,
    remark      TEXT,
    del_flag    INTEGER DEFAULT 0
);

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              1,
                              1,
                              '男',
                              '0',
                              'sys_user_sex',
                              '',
                              '',
                              'Y',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '性别男',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              2,
                              2,
                              '女',
                              '1',
                              'sys_user_sex',
                              '',
                              '',
                              'N',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '性别女',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              3,
                              3,
                              '未知',
                              '2',
                              'sys_user_sex',
                              '',
                              '',
                              'N',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '性别未知',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              4,
                              1,
                              '显示',
                              '0',
                              'sys_show_hide',
                              '',
                              'primary',
                              'Y',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '显示菜单',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              5,
                              2,
                              '隐藏',
                              '1',
                              'sys_show_hide',
                              '',
                              'danger',
                              'N',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '隐藏菜单',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              6,
                              1,
                              '正常',
                              '0',
                              'sys_normal_disable',
                              '',
                              'primary',
                              'Y',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '正常状态',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              7,
                              2,
                              '停用',
                              '1',
                              'sys_normal_disable',
                              '',
                              'danger',
                              'N',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '停用状态',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              8,
                              1,
                              '是',
                              'Y',
                              'sys_yes_no',
                              '',
                              'primary',
                              'Y',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '系统默认是',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              9,
                              2,
                              '否',
                              'N',
                              'sys_yes_no',
                              '',
                              'danger',
                              'N',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '系统默认否',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              10,
                              1,
                              '成功',
                              '0',
                              'sys_common_status',
                              '',
                              'primary',
                              'N',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '正常状态',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              11,
                              2,
                              '失败',
                              '1',
                              'sys_common_status',
                              '',
                              'danger',
                              'N',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '停用状态',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              12,
                              0,
                              'GET',
                              'GET',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842414507,
                              1,
                              1574842414507,
                              'http请求方法',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              13,
                              1,
                              'HEAD',
                              'HEAD',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842439909,
                              1,
                              1574842439909,
                              'http请求方法',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              14,
                              2,
                              'POST',
                              'POST',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842448104,
                              1,
                              1574842448104,
                              'http请求方法',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              15,
                              3,
                              'PUT',
                              'PUT',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842456031,
                              1,
                              1574842456031,
                              'http请求方法',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              16,
                              4,
                              'PATCH',
                              'PATCH',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842473821,
                              1,
                              1574842473821,
                              'http请求方法',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              17,
                              5,
                              'DELETE',
                              'DELETE',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842482767,
                              1,
                              1574842482767,
                              'http请求方法',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              18,
                              6,
                              'OPTIONS',
                              'OPTIONS',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842497976,
                              1,
                              1574842497976,
                              'http请求方法',
                              0
                          );

INSERT INTO sys_dict_data (
                              dict_code,
                              dict_sort,
                              dict_label,
                              dict_value,
                              dict_type,
                              css_class,
                              list_class,
                              is_default,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              19,
                              7,
                              'TRACE',
                              'TRACE',
                              'http_method',
                              '',
                              '',
                              '',
                              1,
                              1574842506060,
                              1,
                              1574842506060,
                              'http请求方法',
                              0
                          );


-- 表：sys_dict_type
DROP TABLE IF EXISTS sys_dict_type;

CREATE TABLE sys_dict_type (
    dict_id     INTEGER PRIMARY KEY AUTO_INCREMENT,
    dict_name   TEXT,
    dict_type   INTEGER,
    create_by   INTEGER,
    create_time INTEGER,
    update_by   INTEGER,
    update_time INTEGER,
    remark      INTEGER,
    del_flag    INTEGER DEFAULT 0
);

INSERT INTO sys_dict_type (
                              dict_id,
                              dict_name,
                              dict_type,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              1,
                              '用户性别',
                              'sys_user_sex',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '用户性别列表',
                              NULL
                          );

INSERT INTO sys_dict_type (
                              dict_id,
                              dict_name,
                              dict_type,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              2,
                              '菜单状态',
                              'sys_show_hide',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '菜单状态列表',
                              NULL
                          );

INSERT INTO sys_dict_type (
                              dict_id,
                              dict_name,
                              dict_type,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              3,
                              '系统开关',
                              'sys_normal_disable',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '系统开关列表',
                              NULL
                          );

INSERT INTO sys_dict_type (
                              dict_id,
                              dict_name,
                              dict_type,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              4,
                              '系统是否',
                              'sys_yes_no',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '系统是否列表',
                              NULL
                          );

INSERT INTO sys_dict_type (
                              dict_id,
                              dict_name,
                              dict_type,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              5,
                              '系统状态',
                              'sys_common_status',
                              1,
                              '2018-03-16 11:33:00',
                              1,
                              '2018-03-16 11:33:00',
                              '登录状态列表',
                              NULL
                          );

INSERT INTO sys_dict_type (
                              dict_id,
                              dict_name,
                              dict_type,
                              create_by,
                              create_time,
                              update_by,
                              update_time,
                              remark,
                              del_flag
                          )
                          VALUES (
                              6,
                              '请求方式',
                              'http_method',
                              1,
                              1574841344757,
                              1,
                              1574841344757,
                              '',
                              0
                          );


-- 表：sys_menu
DROP TABLE IF EXISTS sys_menu;

CREATE TABLE sys_menu (
    menu_id      INTEGER NOT NULL
                         PRIMARY KEY AUTO_INCREMENT,
    menu_name    TEXT,
    parent_id    INTEGER,
    order_num    INTEGER,
    url          TEXT,
    target       TEXT,
    menu_type    INTEGER,
    visible      INTEGER,
    icon         TEXT,
    create_by    INTEGER,
    create_time  INTEGER,
    update_by    INTEGER,
    update_time  INTEGER,
    remark       TEXT,
    user_display INTEGER DEFAULT 1,
    del_flag     INTEGER DEFAULT 0
);

INSERT INTO sys_menu (
                         menu_id,
                         menu_name,
                         parent_id,
                         order_num,
                         url,
                         target,
                         menu_type,
                         visible,
                         icon,
                         create_by,
                         create_time,
                         update_by,
                         update_time,
                         remark,
                         user_display,
                         del_flag
                     )
                     VALUES (
                         1,
                         '用户管理',
                         0,
                         1,
                         '/system/user',
                         'menuItem',
                         'C',
                         0,
                         'fa fa-user',
                         1,
                         1574406396326,
                         1,
                         1574406396326,
                         '用户管理菜单',
                         0,
                         0
                     );

INSERT INTO sys_menu (
                         menu_id,
                         menu_name,
                         parent_id,
                         order_num,
                         url,
                         target,
                         menu_type,
                         visible,
                         icon,
                         create_by,
                         create_time,
                         update_by,
                         update_time,
                         remark,
                         user_display,
                         del_flag
                     )
                     VALUES (
                         2,
                         '系统参数',
                         0,
                         7,
                         '/system/config',
                         'menuItem',
                         'C',
                         0,
                         'fa fa-cogs',
                         1,
                         1574406396326,
                         1,
                         1574406396326,
                         '参数设置菜单',
                         0,
                         0
                     );

INSERT INTO sys_menu (
                         menu_id,
                         menu_name,
                         parent_id,
                         order_num,
                         url,
                         target,
                         menu_type,
                         visible,
                         icon,
                         create_by,
                         create_time,
                         update_by,
                         update_time,
                         remark,
                         user_display,
                         del_flag
                     )
                     VALUES (
                         3,
                         '接口日志',
                         0,
                         2,
                         '/system/log',
                         'menuItem',
                         'C',
                         0,
                         'fa fa-book',
                         1,
                         1574406396326,
                         1,
                         1574406396326,
                         '操作日志菜单',
                         1,
                         0
                     );

INSERT INTO sys_menu (
                         menu_id,
                         menu_name,
                         parent_id,
                         order_num,
                         url,
                         target,
                         menu_type,
                         visible,
                         icon,
                         create_by,
                         create_time,
                         update_by,
                         update_time,
                         remark,
                         user_display,
                         del_flag
                     )
                     VALUES (
                         4,
                         '接口一览',
                         0,
                         1,
                         '/system/url',
                         'menuItem',
                         'C',
                         0,
                         'fa fa-cloud',
                         1,
                         1574406396326,
                         1,
                         1574406396326,
                         'url路径菜单',
                         1,
                         0
                     );


-- 表：sys_user
DROP TABLE IF EXISTS sys_user;

CREATE TABLE sys_user (
    user_id     INTEGER NOT NULL
                        PRIMARY KEY AUTO_INCREMENT,
    login_name  TEXT    NOT NULL,
    user_name   TEXT,
    user_type   INTEGER DEFAULT 0,
    email       TEXT,
    phonenumber TEXT,
    sex         INTEGER,
    avatar      TEXT,
    password    TEXT,
    salt        TEXT,
    status      INTEGER DEFAULT 0,
    del_flag    INTEGER DEFAULT 0,
    login_ip    TEXT,
    login_date  INTEGER,
    create_by   INTEGER,
    create_time INTEGER,
    update_by   INTEGER,
    update_time INTEGER,
    remark      TEXT
);

INSERT INTO sys_user (
                         user_id,
                         login_name,
                         user_name,
                         user_type,
                         email,
                         phonenumber,
                         sex,
                         avatar,
                         password,
                         salt,
                         status,
                         del_flag,
                         login_ip,
                         login_date,
                         create_by,
                         create_time,
                         update_by,
                         update_time,
                         remark
                     )
                     VALUES (
                         1,
                         'vmock',
                         '超级管理员',
                         1,
                         '',
                         '13999999999',
                         1,
                         '',
                         '568611dc5efd140df77f58d5ae7c84f6',
                         'fc4a41',
                         0,
                         0,
                         '0:0:0:0:0:0:0:1',
                         1576576125897,
                         0,
                         1574406396326,
                         1,
                         1576576125898,
                         '管理员，能多看见【用户管理】和【系统参数】'
                     );


-- 索引：idx_url_id_method
DROP INDEX IF EXISTS idx_url_id_method;

CREATE UNIQUE INDEX idx_url_id_method ON mock_response_restful (
    "url_id" COLLATE BINARY ASC,
    "http_method" COLLATE BINARY ASC
);