package com.vmock.biz.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.text.AntPathMatcher;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vmock.base.constant.CommonConst;
import com.vmock.base.dto.PathResultVo;
import com.vmock.base.login.UserContext;
import com.vmock.biz.entity.UrlLogic;
import com.vmock.biz.mapper.UrlLogicMapper;
import com.vmock.biz.service.IUrlLogicService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * 子路径Service业务层处理
 *
 * @author mock
 * @date 2019-11-20
 */
@Service
@RequiredArgsConstructor
public class UrlLogicServiceImpl extends ServiceImpl<UrlLogicMapper, UrlLogic> implements IUrlLogicService {

    private final UrlLogicMapper mockUrlLogicMapper;
    private final static AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    /**
     * 根据url批量插入逻辑表
     *
     * @param url 用户输入的url
     * @return 插入后的子urls
     */
    @Override
    public List<UrlLogic> insertByUrl(String url, Long urlId) {
        // 根据斜线分割 插入逻辑表
        String[] subUrls = url.split("\\/");
        List<UrlLogic> mockUrlLogics = new ArrayList<>();
        // 做成集合批量插入
        //  db sub_url是 UNIQUE，并 ON CONFLICT IGNORE，不会重复。
        //查询是否存在
        Long userId = UserContext.getUserId();
        List<UrlLogic> urlLogic = this.list(Wrappers.lambdaQuery(UrlLogic.class)
                .in(UrlLogic::getSubUrl, Arrays.asList(subUrls))
                .eq(UrlLogic::getCreateBy, userId)
                .eq(UrlLogic::getUrlId, urlId)
        );
        Map<String, Long> logicMap = urlLogic.stream().collect(Collectors.toMap(UrlLogic::getSubUrl, UrlLogic::getLogicId));
        UrlLogic mockUrlLogic;
        for (String subUrl : subUrls) {
            // 空 不记录
            if (StrUtil.isBlank(subUrl)) {
                continue;
            }
            // 存入集合
            mockUrlLogic = new UrlLogic();
            //查询是否存在
            mockUrlLogic.setLogicId(logicMap.get(subUrl));
            mockUrlLogic.setSubUrl(subUrl);
            mockUrlLogic.setUrlId(urlId);
            mockUrlLogics.add(mockUrlLogic);
        }
        this.saveOrUpdateBatch(mockUrlLogics);
        return mockUrlLogics;
    }

    /**
     * 通过url查询logic字符串
     *
     * @param pathResultVo 路径
     * @return logic字符串
     */
    @Override
    public String selectLogicStrByUrl(PathResultVo pathResultVo) {
        // 拆分为子url
        String resultVoUrl = pathResultVo.getUrl();

        List<String> subUrls = StrUtil.splitTrim(resultVoUrl, StrUtil.C_SLASH);
        // empty -> put '/'
        if (subUrls.isEmpty()) {
            subUrls.add(StrUtil.SLASH);
        }
        //增加通配符 *
        subUrls.add(CommonConst.PATH_PLACEHOLDER);
        // 查询对应logicId
        List<UrlLogic> urlLogics = this.list(Wrappers.<UrlLogic>lambdaQuery()
                // in 查询请求的url 这里需要加上url的Path地址
                .in(UrlLogic::getSubUrl, subUrls)
                .eq(UrlLogic::getCreateBy, pathResultVo.getUserId())
        );

        // 通过逻辑来分组
        Map<Long, List<UrlLogic>> urlIdGroupMap = urlLogics.stream().collect(Collectors.groupingBy(UrlLogic::getUrlId));
        AtomicReference<String> finalLogicUrl = new AtomicReference<>(resultVoUrl);

        urlIdGroupMap.forEach((logicId, logicList) -> {
            List<String> urlLogicList = logicList.stream()
                    .sorted(Comparator.comparingLong(UrlLogic::getLogicId))
                    .map(UrlLogic::getSubUrl)
                    .collect(Collectors.toList());
            String joined = CollectionUtil.join(urlLogicList, "", StrUtil.SLASH, null);
            boolean isMatch = PATH_MATCHER.match(joined, resultVoUrl);
            if (isMatch) {
                finalLogicUrl.set(joined);
            }
        });

       return finalLogicUrl.get();
    }
}
