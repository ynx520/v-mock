package com.vmock.base.core.url;

import com.vmock.base.dto.PathResultVo;
import com.vmock.biz.entity.Url;

/**
 * 获取mock url实体类责任链
 *
 * @author vt
 * @since
 */
public abstract class BaseMockUrlHandler {


    /**
     * 下一个处理器
     */
    protected BaseMockUrlHandler nextHandler;

    /**
     * 获取MockUrl实体
     *
     * @param pathResultVo 请求路径
     * @return 命中的实体类
     */
    public Url getMockUrlEntity(PathResultVo pathResultVo) {
        //根据请求Url直接查询实体Url信息
        Url mockUrlEntity = this.find(pathResultVo);
        if (mockUrlEntity != null) {
            return mockUrlEntity;
        }
        if (nextHandler != null) {
            return nextHandler.getMockUrlEntity(pathResultVo);
        }
        return null;
    }

    /**
     * 填入下一个handle
     */
    protected void setNextLogger(BaseMockUrlHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    /**
     * 查找 or 处理 具体逻辑
     *
     * @param pathResultVo 请求路径
     * @return
     */
    abstract protected Url find(PathResultVo pathResultVo);

}
