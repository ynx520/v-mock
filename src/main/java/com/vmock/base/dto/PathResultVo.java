package com.vmock.base.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 从地址栏解析出来的VO
 *
 * @author mock
 */
@Data
@Builder
@Accessors(chain = true)
public class PathResultVo {
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 每页显示记录数
     */
    private String url;

}
