package com.vmock.base.config;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.vmock.base.login.UserContext;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * common field injector
 * 自动填充创建人 创建日期 修改人 修改日期等公共字段
 *
 * @author vt
 * @since 2019年11月26日
 */
@Component
public class CustomFillHandler implements MetaObjectHandler {

    /**
     * 创建人字段
     */
    private static final String CREATE_BY = "createBy";

    /**
     * 创建时间字段
     */
    private static final String CREATE_TIME = "createTime";

    /**
     * 更新人字段
     */
    private static final String UPDATE_BY = "updateBy";

    /**
     * 更新时间字段
     */
    private static final String UPDATE_TIME = "updateTime";

    /**
     * 插入方法共通填充
     *
     * @param metaObject 元对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        LocalDateTime currentTime = LocalDateTime.now();
        // create time
        this.setFieldValByName(CREATE_TIME, currentTime, metaObject);
        // update time
        this.setFieldValByName(UPDATE_TIME, currentTime, metaObject);

        //异步环境就不插入用户信息了
        if (ObjectUtil.isNotEmpty(UserContext.getUserId())){
            // create user id
            this.setFieldValByName(CREATE_BY, String.valueOf(UserContext.getUserId()), metaObject);
            // update user id
            this.setFieldValByName(UPDATE_BY, String.valueOf(UserContext.getUserId()), metaObject);
        }
    }

    /**
     * 更新方法共通填充
     *
     * @param metaObject 元对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        // update time
        this.setFieldValByName(UPDATE_TIME, LocalDateTime.now(), metaObject);
        // update user id
        this.setFieldValByName(UPDATE_BY, String.valueOf(UserContext.getUserId()), metaObject);
    }
}
